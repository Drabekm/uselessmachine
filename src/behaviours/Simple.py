from behaviours import BaseBehaviour

class Simple(BaseBehaviour.BaseBehaviour):

    switchOffCount = 0
    switchOffCountLimit = 5
    def Process(self):
        speed = 5
        currentSwitchValue = self.switch.value()

        if currentSwitchValue == 1:
            self.angle += speed
        else:
            self.angle -= speed

        if self._SwitchJustTurnedOff():
            self.switchOffCount += 1
            print(self.switchOffCount)

        self.previousSwitchValue = self.switch.value()        
        self._SetAngle(self.angle)

        if self.switchOffCount >= self.switchOffCountLimit:
            return True