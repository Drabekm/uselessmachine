from behaviours import BaseBehaviour

class ArmReset(BaseBehaviour.BaseBehaviour):

    def Process(self):
        speed = 10
        self.angle -= speed

        self._SetAngle(self.angle)
        if (self.angle == 0):
            return True