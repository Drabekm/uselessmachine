from machine import Pin,PWM

class BaseBehaviour:

    __SERVO_MIN = 1638
    __SERVO_MAX = 8191 / 2 # Hardware max is 8191

    currentServoDuty = 1638

    def __init__(self, servo: PWM, switch: Pin, isFlipped: bool):
        self.servo = servo
        self.switch = switch
        self.isFlipped = isFlipped
        self.angle = 0
        self.previousSwitchValue = self.switch.value()

    def _EnsureAngleLimit(self, angle) -> None:
        minAngle = 0
        maxAngle = 180
        self.angle = max(minAngle, min(angle, maxAngle))

    def _SwitchJustTurnedOff(self) -> bool:
        return self.previousSwitchValue == 1 and self.switch.value() == 0

    def _SetAngle(self, angle) -> None:
        self._EnsureAngleLimit(angle)
        
        duty = self._MapAngleToDuty(self.angle)
        self._SetDuty(duty)

    def _MapAngleToDuty(self, angle) -> int:
        possibleDutyRange = self.__SERVO_MAX - self.__SERVO_MIN
        dutyForOneDegree = possibleDutyRange / 180

        if self.isFlipped:
            return int(self.__SERVO_MAX - (dutyForOneDegree * angle))
        else:
            return int(self.__SERVO_MIN + (dutyForOneDegree * angle))

    def _SetDuty(self, duty) -> None:
        self.servo.duty_u16(int(duty))

    def Process(self):
        pass