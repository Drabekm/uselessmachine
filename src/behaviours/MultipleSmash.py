from behaviours import BaseBehaviour

class MultipleSmash(BaseBehaviour.BaseBehaviour):

    angerSmashLimit = 4 # TODO make random
    angerSmashCount = 0

    # Anger mode variables
    movingToMax = False
    movingToIntermediate = False

    angerDirection = 1

    def __init__(self, servo, switch):
        super().__init__(servo, switch)
        self.servoIntermediateDuty = self.__SERVO_MAX - 3000 # Servo duty when the anger movement stops and moves back
        self.AngerMode: bool = False

    def Process(self):
        self._EnsureAngleLimit()

        if self.AngerMode:
            self.__AngerMode() # anger mode doesn't check for switch state, it's angy and smashing the switch
        else:
            self.__NormalMode() # normal mode works similiar to simple behaviour
        
        self._SetDuty()

        if self.angerSmashCount >= self.angerSmashLimit:
            return True
    
    def __NormalMode(self):
        speed = 150
        currentSwitchValue = self.switch.value()

        if currentSwitchValue == 1:
            self.currentServoDuty += speed
        else:
            self.currentServoDuty -= speed

        if self._SwitchJustTurnedOff():
            self.AngerMode = True
            print("Anger mode starting")

        self.previousSwitchValue = self.switch.value()

    def __AngerMode(self):
        speed = 100

        if (self.currentServoDuty >= self.__SERVO_MAX):
            self.angerDirection = -1 # move down
            self.angerSmashCount = self.angerSmashCount + 1
            print("Smash count" + str(self.angerSmashCount) + " | Current duty: " + str(self.currentServoDuty))
        elif (self.currentServoDuty <= self.servoIntermediateDuty):
            self.angerDirection = 1 # move up
        
        self.currentServoDuty = self.currentServoDuty + (speed * self.angerDirection)        
        # print(self.currentServoDuty)