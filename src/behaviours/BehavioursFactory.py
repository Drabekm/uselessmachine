from behaviours.ArmReset import ArmReset
from behaviours.MultipleSmash import MultipleSmash
from behaviours.Simple import Simple
from machine import Pin,PWM

class BehavioursFactory:
    def __init__(self, servo: PWM, switch: Pin, isFlipped: bool):
        self.servo = servo
        self.switch = switch
        self.isFlipped = isFlipped
    
    def GetSimple(self):
        print("Simple behaviour created")
        return Simple(self.servo, self.switch, self.isFlipped)
    
    def GetArmReset(self):
        print("ArmReset behaviour created")
        return ArmReset(self.servo, self.switch, self.isFlipped)
    
    def GetMultipleSmash(self):
        print("MultipleSmash (angry) behaviour created")
        return MultipleSmash(self.servo, self.switch, self.isFlipped)