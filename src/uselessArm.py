from behaviours.ArmReset import ArmReset
from behaviours.BaseBehaviour import BaseBehaviour
from behaviours.BehavioursFactory import BehavioursFactory
from machine import Pin,PWM
from behaviours import Simple
import random

class UselessArm:

    def __init__(self, switchPin: Pin, servoPin: Pin, isFlipped: bool) -> None:

        # Pin setup
        self.switch = switchPin
        self.switch.init(Pin.IN, Pin.PULL_UP)

        servoPin.init(Pin.OUT)
        self.servo = PWM(servoPin)
        self.servo.freq(50)
        # self.servo.duty_ns(self.__SERVO_MIN)
        random.seed()
        # Behaviours setup
        self.behavioursFactory = BehavioursFactory(self.servo, self.switch, isFlipped)

        # First behaviour
        self.currentBehaviour = self.behavioursFactory.GetSimple()

    def __GetRandomBehaviour(self) -> None:
        # TODO: Add random behaviour generator
        randomValue = random.randint(0, 100)
        print ("Random value: ")
        print (randomValue)

        if (randomValue < 20):
            self.currentBehaviour = self.behavioursFactory.GetSimple()
        else:
            self.currentBehaviour = self.behavioursFactory.GetMultipleSmash()

    def __ResetArm(self) -> None:
        self.currentBehaviour = self.behavioursFactory.GetArmReset()

    def GetNextBehaviour(self) -> None:
        if isinstance(self.currentBehaviour, ArmReset):
            # self.__GetRandomBehaviour() TODO: JUST FOR TEST
            self.currentBehaviour = self.behavioursFactory.GetSimple()
        else:
            self.__ResetArm()

    def SetAngle(self, angle):
        self.currentBehaviour._SetAngle(angle)

    def Process(self) -> None:
        if self.currentBehaviour.Process():          
            print("Behaviour done!")
            self.GetNextBehaviour()
        
        # print(self.servo.duty_u16())