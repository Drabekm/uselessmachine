from machine import Pin,PWM

class Useless:
    __SERVO_MIN = 500000
    __SERVO_MAX = 2500000

    leftSwitch:Pin = None
    rightSwitch:Pin = None

    leftServo:PWM = None
    rightSwitch:PWM = None

    def __init__(self, leftSwitchPin: Pin, rightSwitchPin: Pin, leftServoPin: Pin, rightServoPin: Pin) -> None:
        self.leftSwitch = leftSwitchPin
        self.rightSwitch = rightSwitchPin

        self.leftServo = PWM(leftServoPin)
        self.leftServo.freq(50)

        self.rightServo = PWM(rightServoPin)
        self.rightServo.freq(50)
    
    def ExtrudeServo(self, left: bool) -> None:
        if left:
            self.leftServo.duty_ns(self.__SERVO_MAX)
        else:
            self.rightServo.duty_ns(self.__SERVO_MAX)
    
    def RetractServo(self, left: bool) -> None:
        if left:
            self.leftServo.duty_ns(self.__SERVO_MIN)
        else:
            self.rightServo.duty_ns(self.__SERVO_MIN)