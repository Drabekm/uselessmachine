from machine import Pin
from uselessMachine import Useless
from uselessArm import UselessArm
import utime


arm = UselessArm(Pin(3, Pin.IN, Pin.PULL_UP), Pin(4), True)
# arm2 = UselessArm(Pin(7, Pin.IN, Pin.PULL_UP), Pin(6))
arm2 = UselessArm(Pin(7, Pin.IN, Pin.PULL_UP), Pin(6), False)

# arm.SetAngle(190)
# arm2.SetAngle(0)

while True:
    arm.Process()
    arm2.Process()
    utime.sleep_ms(10)